package ribomation.rest_ws.server;

import ribomation.rest_ws.domain.Product;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class ProductsRepo {
    private static final String        SEP         = ";";
    private static final String        productsCSV = "/products.csv";
    private static final AtomicLong    nextId      = new AtomicLong(1);
    public static final  ProductsRepo  instance    = new ProductsRepo();
    private              List<Product> products;

    static {
        instance.load();
    }

    public void load() {
        products = Collections.synchronizedList(load(productsCSV));
    }

    public void dump() {
        products.forEach(System.out::println);
    }

    public List<Product> all() {
        return products;
    }

    public Optional<Product> findById(long id) {
        synchronized (products) {
            return products.stream()
                    .filter(p -> p.getId() == id)
                    .findFirst();
        }
    }

    public Product insert(Product p) {
        p.setId(nextId.getAndIncrement());
        if (p.getName() == null) throw new IllegalArgumentException("missing name");
        if (p.getModified() == null) p.setModified(new Date());
        if (p.getPrice() == 0) p.setPrice(123);
        if (p.getItems() == 0) p.setItems(10);
        products.add(p);
        return p;
    }

    public Product update(long id, Product delta) {
        synchronized (products) {
            Product p = findById(id).orElseThrow();
            if (delta.getName() != null) {
                p.setName(delta.getName());
            }
            if (delta.getPrice() > 0) {
                p.setPrice(delta.getPrice());
            }
            if (delta.getItems() > 0) {
                p.setItems(delta.getItems());
            }
            if (delta.getModified() != null) {
                p.setModified(delta.getModified());
            }
            return p;
        }
    }

    public void remove(long id) {
        synchronized (products) {
            products.removeIf(p -> p.getId() == id);
        }
    }

    List<Product> load(String classpathResource) {
        InputStream is = getClass().getResourceAsStream(classpathResource);
        if (is == null) {
            throw new RuntimeException("cannot open " + classpathResource);
        }

        return new BufferedReader(new InputStreamReader(is)).lines()
                .skip(1)
                .map(csv -> fromCSV(csv, SEP))
                .collect(Collectors.toList());
    }

    Product fromCSV(String csv, String sep) {
        try {
            String[] fields   = csv.split(sep);
            long     id       = nextId.getAndIncrement();
            String   name     = fields[0].replaceAll("\"", "");
            float    price    = Float.parseFloat(fields[1]);
            int      items    = Integer.parseInt(fields[2]);
            Date     modified = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fields[3]);

            return new Product(id, name, price, items, modified);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
