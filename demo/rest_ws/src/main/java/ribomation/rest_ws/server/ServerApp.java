package ribomation.rest_ws.server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ribomation.rest_ws.domain.Product;

import java.io.IOException;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.logging.LogManager;

import static java.lang.System.Logger.Level.INFO;
import static java.lang.System.Logger.Level.WARNING;
import static spark.Spark.*;

public class ServerApp {
    final System.Logger LOG  = System.getLogger("REST Server");
    final int           PORT = 9000;
    final String        URI  = "/api/products";
    final String        JSON = "application/json";
    final Gson          GSON = new GsonBuilder()
            .setPrettyPrinting()
            .setDateFormat("yyyy-MM-dd HH:mm:ss")
            .create();

    public static void main(String[] args) throws IOException {
        LogManager.getLogManager()
                .readConfiguration(ServerApp.class.getResourceAsStream("/logging.properties"));

        ServerApp app = new ServerApp();
        app.config();
        app.interceptors();
        app.routes();
        app.ready();
    }

    void config() {
        port(PORT);
    }

    void interceptors() {
        exception(NoSuchElementException.class, (ex, req, res) -> {
            LOG.log(WARNING, "Not found: {0}", ex.getMessage());
            res.status(404);
        });

        exception(IllegalArgumentException.class, (ex, req, res) -> {
            LOG.log(WARNING, "Malformed request", ex);
            res.status(404);
        });

        notFound((req, res) -> {
            String msg = String.format("Invalid REQ: %s %s", req.requestMethod(), req.uri());
            LOG.log(WARNING, msg);
            res.status(404);
            return "";
        });

        before((req, res) -> {
            LOG.log(INFO, "REQ: {0} {1}", req.requestMethod(), req.uri());
        });

        after((req, res) -> res.type(JSON));
    }

    void ready() {
        LOG.log(INFO, "started at http://localhost:{0,number,#}{1}", PORT, URI);
    }

    void routes() {
        get("/", (req, res) -> {
            res.redirect(URI);
            return null;
        });

        post(URI, JSON, (req, res) -> {
            Product p = GSON.fromJson(req.body(), Product.class);
            if (p == null) halt(400);

            res.status(201);
            return ProductsRepo.instance.insert(p);
        }, GSON::toJson);

        get(URI, JSON, (req, res) -> {
            return ProductsRepo.instance.all();
        }, GSON::toJson);

        get(URI + "/:id", JSON, (req, res) -> {
            long              id    = Long.parseLong(req.params("id"));
            Optional<Product> maybe = ProductsRepo.instance.findById(id);
            if (maybe.isPresent()) {
                return maybe.get();
            } else {
                res.status(404);
                return Map.of("not-found", id);
            }
        }, GSON::toJson);

        put(URI + "/:id", JSON, (req, res) -> {
            long              id    = Long.parseLong(req.params("id"));
            Optional<Product> maybe = ProductsRepo.instance.findById(id);
            if (maybe.isPresent()) {
                return ProductsRepo.instance.update(id, GSON.fromJson(req.body(), Product.class));
            } else {
                res.status(404);
                return Map.of("not-found", id);
            }
        }, GSON::toJson);

        delete(URI + "/:id", JSON, (req, res) -> {
            long              id    = Long.parseLong(req.params("id"));
            Optional<Product> maybe = ProductsRepo.instance.findById(id);
            if (maybe.isPresent()) {
                ProductsRepo.instance.remove(id);
                res.status(204);
                return Map.of("deleted", "ok");
            } else {
                res.status(404);
                return Map.of("not-found", id);
            }
        }, GSON::toJson);
    }

}
