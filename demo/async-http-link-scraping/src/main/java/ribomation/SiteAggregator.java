package ribomation;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.net.http.HttpClient.Redirect.NORMAL;
import static java.net.http.HttpResponse.BodyHandlers.ofString;
import static java.time.Duration.ofSeconds;

public class SiteAggregator {
    final static String baseUrl = "https://www.ribomation.com";

    public static void main(String[] args) {
        SiteAggregator app = new SiteAggregator();
        app.pipeline(baseUrl + "/index.html");
    }

    void pipeline(String indexPageUrl) {
        long startTime = System.nanoTime();
        System.out.printf("Fetching links from %s ...%n", indexPageUrl);

        var httpClient = client();
        httpClient.sendAsync(GET(indexPageUrl), ofString())
                .thenApply(HttpResponse::body)
                .thenApply((String indexPage) ->
                        Jsoup.parse(indexPage, baseUrl)
                                .select("#courses-list a.course")
                                .stream()
                                .map(link -> link.attr("href"))
                                .filter(url -> !url.endsWith("index.html"))
                                .map(uri -> baseUrl + uri)
                                .peek(System.out::println)
                                .map(url -> httpClient
                                        .sendAsync(GET(url), ofString())
                                        .thenApply(HttpResponse::body)
                                        .thenApply((String coursePage) -> {
                                            Document doc      = Jsoup.parse(coursePage, baseUrl);
                                            String   name     = doc.select("#facts th:contains(Name) + td").text();
                                            String   duration = doc.select("#facts th:contains(Duration) + td").text();
                                            String   ingress  = doc.select(".ingress").text();
                                            return Map.of(
                                                    "name", name,
                                                    "duration", duration,
                                                    "ingress", ingress.substring(0, Math.min(ingress.length(), 100))
                                            );
                                        })
                                )
                )
                .thenApply((Stream<CompletableFuture<Map<String, String>>> data) ->
                        data.map(CompletableFuture::join)
                )
                .thenApply((Stream<Map<String, String>> courses) -> {
                    List<Map<String, String>> list = courses.collect(Collectors.toList());
                    int maxWidth = list.stream()
                            .mapToInt(data -> data.get("name").length())
                            .max().orElse(25);
                    return list.stream()
                            .map(course -> String.format("%-" + maxWidth + "s (%s): %s",
                                    course.get("name"), course.get("duration"), course.get("ingress")
                            ))
                            .collect(Collectors.toList());
                })
                .thenApply((List<String> courseList) -> {
                    courseList.forEach(System.out::println);
                    return courseList.size();
                })
                .thenAccept((Integer numCourses) -> {
                    System.out.println("-------------");
                    System.out.printf("Processed: %d course pages%n", numCourses);
                    System.out.printf("Elapsed  : %.3f seconds%n", 1E-9 * (System.nanoTime() - startTime));
                })
                .join();
    }

    HttpClient client() {
        return HttpClient.newBuilder()
                .followRedirects(NORMAL)
                .connectTimeout(ofSeconds(10))
                .build();
    }

    HttpRequest GET(String url) {
        return HttpRequest.newBuilder()
                .uri(URI.create(url))
                .GET()
                .build();
    }

}
