# Installation Instructions

In order to participate and perform the programming exercises of the course, 
you need to have the following installed.

## Zoom Client
* https://us02web.zoom.us/download
* [Video/Audio Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md)

## GIT Client
* https://git-scm.com/downloads

## IDE
A decent IDE, such as any of

* JetBrains IntellJ IDEA (*Community*)
    - https://www.jetbrains.com/idea/download
* MicroSoft Visual Code
    - https://code.visualstudio.com/download


## Java JDK
Install SDKMAN https://sdkman.io/ to GIT BASH

    curl -s "https://get.sdkman.io" | bash
    source "$HOME/.sdkman/bin/sdkman-init.sh"
    sdk version

and use it to install the latest version of Java

    sdk install java

More info https://sdkman.io/usage

