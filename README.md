# Java Modern, from version 8 and up
### 20-22 June 2022

# Links
* [Installation Instructions](./installation-instructions.md)
* [Zoom Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md)
* [Course Details](https://www.ribomation.se/programmerings-kurser/jvm/java-modern/)


# Usage of this GIT Repo
Ensure you have a [GIT client](https://git-scm.com/downloads) installed, open a GIT BASH terminal window  and type the commands below to clone this repo. 

    mkdir -p ~/java-course/my-solutions
    cd ~/java-course
    git clone <git https url> gitlab

![GIT HTTPS URL](./git-url.png)

During the course, solutions will be push:ed to this repo and you can get these by a `git pull` operation

    cd ~/java-course/gitlab
    git pull

# Build Solution/Demo Programs
All programs are ordinary Java programs and can be compiled using any appropriate tool.
Several of the demo & solutions programs has a Gradle build file and can therefore be built by

    ./gradlew build         # BASH
    gradlew.bat build       # Windows



# Links to Large Files
For some of the exercises, you might want to use a large input file. Here are some compressed large text files to use.
* [English Text, 100MB (_38MB compressed_)](https://docs.ribomation.se/java/java-8/english.100MB.gz)
* [English Text, 1024MB (_396MB compressed_)](https://docs.ribomation.se/java/java-8/english.1024MB.gz)
* [Climate Data, 2,5GB (_432MB compressed_)](https://docs.ribomation.se/java/java-8/climate-data.txt.gz)



***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
